﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadBumper : MonoBehaviour
{
    private GameObject ball;
    private DeadBumper[] group;
    private Light spotLight;
    private bool hit;
    private float restartTimer;
    private bool active;
    // Use this for initialization
    void Start ()
    {
        ball = GameObject.FindGameObjectWithTag("Ball");
        spotLight = gameObject.GetComponentInChildren<Light>();
        group = GetComponentsInParent<DeadBumper>();
        hit = false;
        restartTimer = 5F;
        active = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (hit && active)
        {
            restartTimer -= Time.deltaTime;
            if (restartTimer <= 0)
            {
                hit = false;
                restartTimer = 5F;
                spotLight.color = new Vector4(255, 160, 0, 255);
            }
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == ball)
        {
            spotLight.color = Color.red;
            hit = true;
        }
        if (collision.gameObject.tag == "Field")
        {
            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }

    public bool GetHit()
    {
        return hit;
    }

    private void CheckGroup()
    {
        int hitCount = 0;
        for (int i = 0; i < group.Length; i++)
        {
            if (group[i].GetHit())
            {
                hitCount++;
            }
        }
        if (hitCount == group.Length)
        {
            transform.GetComponentInParent<DBGroup>().GroupLocked();
        }
        return;
    }

    public void SetActive(bool _active)
    {
        active = _active;
        return;
    }
}
