﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutZone : MonoBehaviour {
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            gameObject.GetComponentInParent<GameManager>().OutZoneEnter(other);
        }
    }
}
