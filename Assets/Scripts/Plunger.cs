﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plunger : MonoBehaviour
{
    SpringJoint spring;
    Rigidbody rb;
    [SerializeField] float maxPosition;
    [SerializeField] float descSpeed = 1;
    private bool downPressedDown, downPressed, downPressedUp;
    private GameManager gameManager;

    void Start()
    {
        spring = GetComponent<SpringJoint>();
        rb = GetComponent<Rigidbody>();
        downPressedDown = downPressed = downPressedUp = false;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
        if (!gameManager.GetGameStart()) { return; }
        if (Input.GetKeyDown("space"))
        {
            spring.spring = 1000 + Random.Range(0, 1000);
            downPressedDown = true;
            downPressed = false;
            downPressedUp = false;
        }
        else if (Input.GetKey("space"))
        {
            downPressedDown = false;
            downPressed = true;
            downPressedUp = false;
        }
        else if (Input.GetKeyUp("space"))
        {
            downPressedDown = false;
            downPressed = false;
            downPressedUp = true;
        }
        else
        {
            downPressedDown = false;
            downPressed = false;
            downPressedUp = false;
        }
    }

    void FixedUpdate()
    {
        if (!gameManager.GetGameStart()) { return; }
        if (downPressedDown)
        {
            spring.maxDistance = maxPosition;
            spring.damper = 50;
        }

        if (downPressed)
        {
            rb.AddForce(0, 0, 5, ForceMode.Acceleration);
            spring.damper = 50;
        }

        if (downPressedUp)
        {
            spring.maxDistance = 0;
            spring.damper = 0.025F;
        }
    }

}
