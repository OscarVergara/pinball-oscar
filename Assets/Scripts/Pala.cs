﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pala : MonoBehaviour
{
    AudioSource audioSourceL;
    AudioSource audioSourceR;
    JointSpring spring;
    GameObject[] tempL;
    GameObject[] tempR;
    HingeJoint[] hingeJointLeft;
    HingeJoint[] hingeJointRight;

    [SerializeField] float restPosition = 0F;
    [SerializeField] float pressedPosition = 45F;
    [SerializeField] float flipperStrength = 1000F;
    [SerializeField] float flipperDamper = 100F;
    [SerializeField] float direction;

    float turn;

    // Use this for initialization
    void Start()
    {
        audioSourceL = GameObject.Find("GameManager").transform.GetChild(4).GetComponent<AudioSource>();
        audioSourceR = GameObject.Find("GameManager").transform.GetChild(5).GetComponent<AudioSource>();
        spring = new JointSpring();
        spring.spring = flipperStrength;
        spring.damper = flipperDamper;
        spring.targetPosition = restPosition;

        tempL = GameObject.FindGameObjectsWithTag("Left Flipper");
        hingeJointLeft = new HingeJoint[tempL.Length];
        for (int i = 0; i < hingeJointLeft.Length; i++)
        {
            hingeJointLeft[i] = tempL[i].GetComponent<HingeJoint>();
            hingeJointLeft[i].spring = spring;
            hingeJointLeft[i].useSpring = true;
        }

        tempR = GameObject.FindGameObjectsWithTag("Right Flipper");
        hingeJointRight = new HingeJoint[tempR.Length];
        for (int i = 0; i < hingeJointRight.Length; i++)
        {
            hingeJointRight[i] = tempR[i].GetComponent<HingeJoint>();
            hingeJointRight[i].spring = spring;
            hingeJointRight[i].useSpring = true;
        }

    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            audioSourceL.clip = (AudioClip) Resources.Load("Sounds/FlipperPressSFX", typeof(AudioClip));
            audioSourceL.Play();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            audioSourceR.clip = (AudioClip)Resources.Load("Sounds/FlipperPressSFX", typeof(AudioClip));
            audioSourceR.Play();
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            audioSourceL.clip = (AudioClip)Resources.Load("Sounds/FlipperReleaseSFX", typeof(AudioClip));
            audioSourceL.Play();
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            audioSourceR.clip = (AudioClip)Resources.Load("Sounds/FlipperReleaseSFX", typeof(AudioClip));
            audioSourceR.Play();
        }
    }
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            spring.targetPosition = pressedPosition;
            for (int i = 0; i < hingeJointRight.Length; i++)
            {
                hingeJointRight[i].spring = spring;
            }
        }
        else
        {
            spring.targetPosition = restPosition;
            for (int i = 0; i < hingeJointRight.Length; i++)
            {
                hingeJointRight[i].spring = spring;
            }
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            spring.targetPosition = -pressedPosition;
            for (int i = 0; i < hingeJointLeft.Length; i++)
            {
                hingeJointLeft[i].spring = spring;
            }
        }
        else
        {
            spring.targetPosition = restPosition;
            for (int i = 0; i < hingeJointLeft.Length; i++)
            {
                hingeJointLeft[i].spring = spring;
            }
        }
    }
}