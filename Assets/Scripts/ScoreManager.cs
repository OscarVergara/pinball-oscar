﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    private Text scoreText;
    private int playerScore;
    private int playerLives;
    // Use this for initialization
    void Start()
    {
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        playerLives = 3;
        playerScore = 0;
        scoreText.text = "Score:" + playerScore;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddScore(int _score)
    {
        playerScore += _score;
        scoreText.text = "Score:" + playerScore;
        return;
    }
}