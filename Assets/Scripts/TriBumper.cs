﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriBumper : MonoBehaviour
{
    private ScoreManager scoreManager;
    public float bumperForce;

    private void Start()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            foreach (var contact in collision.contacts)
            {
                contact.otherCollider.GetComponent<Rigidbody>().AddForce(-1 * new Vector3(contact.normal.x, 0, contact.normal.z) * bumperForce, ForceMode.VelocityChange);
                scoreManager.AddScore(150);
            }
        }
    }
}

