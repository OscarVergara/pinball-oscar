﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DBGroup : MonoBehaviour {
    private ScoreManager scoreManager;
    private GameObject[] deadBumpers;
    private bool groupLocked;
	// Use this for initialization
	void Start ()
    {
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
        deadBumpers = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            deadBumpers[i] = transform.GetChild(i).gameObject;
        }
        groupLocked = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (!groupLocked)
        {
            CheckDB();
        }
	}

    private void CheckDB()
    {
        int hitCount = 0;
        for (int i = 0; i < deadBumpers.Length; i++)
        {
            if (deadBumpers[i].GetComponent<DeadBumper>().GetHit())
            {
                hitCount++;
            }
        }
        if (hitCount == deadBumpers.Length)
        {
            GroupLocked();
        }
        return;
    }
    public void GroupLocked()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            deadBumpers[i].SetActive(false);
        }
        groupLocked = true;
        scoreManager.AddScore(300);
        return;
    }
}
