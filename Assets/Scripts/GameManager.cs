﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    private int playerLifes;
    private GameObject OutZone;
    private ScoreManager scoreManager;
    private GameObject ball;
    private Text livesText;
    private Text centerText;
    private bool gameStart;
    // Use this for initialization
    void Start ()
    {
        OutZone = transform.GetChild(0).gameObject;
        playerLifes = 3;
        ball = GameObject.FindGameObjectWithTag("Ball");
        livesText = GameObject.Find("LifesText").GetComponent<Text>();
        livesText.text = "Lifes: " + playerLifes;
        centerText = GameObject.Find("CenterText").GetComponent<Text>();
        centerText.text = "press enter \n to start";
        gameStart = false;
            
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey(KeyCode.Return) && playerLifes > 0)
        {
            centerText.text = "";
            gameStart = true;
        }
	}

    public void OutZoneEnter(Collider col)
    {
        if (playerLifes > 0)
        {
            transform.GetChild(3).GetComponent<AudioSource>().Play();
            playerLifes -= 1;
        }
        livesText.text = "Lifes: " + playerLifes;
        if (playerLifes <= 0) {
            centerText.text = "GAME OVER!";
            return;
        }
         ball.transform.position = new Vector3(0.49F, -2.176F, 23.17F);
         return;
    }

    public bool GetGameStart()
    {
        return gameStart;
    }

}
