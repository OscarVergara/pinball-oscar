﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    AudioSource audioSource;
    private ScoreManager scoreManager;
    private GameObject ball;
    private bool active;
    private float positionTimer1;
    private float positionTimer2;
    private float restartTimer;
    public int killPoints;
	// Use this for initialization
	void Start ()
    {
        audioSource = GameObject.Find("GameManager").transform.GetChild(6).GetComponent<AudioSource>();
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
        ball = GameObject.FindGameObjectWithTag("Ball");
        active = true;
        positionTimer1 = 0.75F;
        positionTimer2 = 0.75F;
        restartTimer = 10;

    }
	
	// Update is called once per frame
	void Update () {
        if (!active)
        {
            if (positionTimer1 > 0)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y - 0.025F, transform.position.z);
                positionTimer1 -= Time.deltaTime;
            }
            if (positionTimer1 <= 0)
            {
                if (restartTimer > 0)
                {
                    restartTimer -= Time.deltaTime;
                }
                if (restartTimer <= 0)
                {
                    if (positionTimer2 > 0)
                    {
                        transform.position = new Vector3(transform.position.x, transform.position.y + 0.025F, transform.position.z);
                        positionTimer2 -= Time.deltaTime;
                    }
                    if (positionTimer2 <= 0)
                    {
                        active = true;
                        positionTimer1 = 0.75F;
                        positionTimer2 = 0.75F;
                        restartTimer = 7F;
                    }
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject == ball && active)
        {
            active = false;
            scoreManager.AddScore(100);
            audioSource.Play();
        }
        if (collision.gameObject.tag == "Field")
        {
            Physics.IgnoreCollision(collision.gameObject.GetComponent<Collider>(), GetComponent<Collider>());
        }
    }
   
}
