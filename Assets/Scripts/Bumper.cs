﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper : MonoBehaviour
{
    AudioSource audioSource;
    ScoreManager scoreManager;
    GameObject ball;
    GameObject light;
    private float lightTimer;
    public float sphereRadius;
    public float explosionRadius;
    public float bumperForce;
    
// Use this for initialization
    void Start () {
        audioSource = GameObject.Find("GameManager").transform.GetChild(1).GetComponent<AudioSource>();
        scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
        ball = GameObject.FindGameObjectWithTag("Ball");
        light = transform.GetChild(1).gameObject;
        lightTimer = 0.25F;
	}
    private void Update()
    {
        if (lightTimer > 0 && light.activeInHierarchy)
        {
            lightTimer -= Time.deltaTime;
            if (lightTimer <= 0)
            {
                light.SetActive(false);
                lightTimer = 0.25F;
            }
        }
    }
    // Update is called once per frame
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            audioSource.Play();
            light.SetActive(true);
            foreach (var contact in collision.contacts)
            {
                contact.otherCollider.GetComponent<Rigidbody>().AddForce(-1 * new Vector3(contact.normal.x, 0, contact.normal.z) * bumperForce, ForceMode.VelocityChange);
                scoreManager.AddScore(200);
            }
        }
    }
    
}
